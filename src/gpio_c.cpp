#include "gpio_c.h"
#include "functions.h"
#include <sstream>
#include <unistd.h>

gpio_c::gpio_c(int gpio_bank, int gpio_number)
{
    gpio_bank_priv = gpio_bank;
    gpio_number_priv = gpio_number;
    exportb();

}

gpio_c::gpio_c(int gpio_bank, int gpio_number, t_dir direction)
{
    gpio_bank_priv = gpio_bank;
    gpio_number_priv = gpio_number;
    exportb();
    set_direction(direction);
}

gpio_c::~gpio_c()
{
    unexport();
}

string gpio_c::get_gpionum(int bank,int number)
{
    ostringstream convert;
    bank = bank  * 32 + number;
    convert << bank;
    return convert.str();
}

void gpio_c::set(int gpio_bank,int gpio_number)
{
    unexport();
    gpio_bank_priv = gpio_bank;
    gpio_number_priv = gpio_number;
}

int gpio_c::exportb(int gpio_bank, int gpio_number)
{
    fstream f;

    f.open(GPIO_PATH  "export",ios_base::out);

    if(f.is_open())
    {
        f << get_gpionum(gpio_bank, gpio_number);
        cout << "Exporting GPIO "<<gpio_bank << "." << gpio_number << " DONE" << endl;
    }
    else
    {
        cerr << "ERROR: Cannot open the export file (B"<< gpio_bank << " P" << gpio_number << ")" << endl;
        return 1;
    }
    f.close();

    return 0;
}

int gpio_c::exportb(int gpio_bank)
{
    fstream f;

    f.open(GPIO_PATH  "export",ios_base::out);

    if(f.is_open())
    {
        for(int i = gpio_bank ; i < gpio_bank + 32; i++)
        {
            f << i;
            cout << "Exporting GPIO "<< gpio_bank << "." << i - gpio_bank << " DONE" << endl;
        }
    }
    else
    {
        cerr << "ERROR: Cannot open the export file" << endl;
        return 1;
    }
    f.close();

    return 0;
}

int gpio_c::exportb()
{
    return exportb(gpio_bank_priv,gpio_number_priv);
}

int gpio_c::unexport(int gpio_bank, int gpio_number)
{
    fstream f;
    f.open(GPIO_PATH  "unexport",ios_base::out);

    if(f.is_open())
    {
        f << get_gpionum(gpio_bank, gpio_number);
        cout << "Unexporting GPIO "<< gpio_bank << "." << gpio_number << " DONE" << endl;
    }
    else
    {
        cerr << "ERROR: Cannot open the unexport file (B"<< gpio_bank << " P" << gpio_number << ")" << endl;
        return 1;
    }
    f.close();

    return 0;
}

int gpio_c::unexport()
{
    return unexport(gpio_bank_priv,gpio_number_priv);
}

int gpio_c::set_direction(int gpio_bank, int gpio_number,t_dir direction)
{
    fstream f;
    string path_direction=GPIO_PATH"gpio";
    // PATH PARSING
    path_direction.append( get_gpionum( gpio_bank,gpio_number ) );
    path_direction.append("/direction");
    // END

    f.open(path_direction.c_str(),ios_base::out);

    if(f.is_open())
    {
        if(direction == DIR_OUT)
        {
            f << "out";
            cout << "Setting direction OUT"<< gpio_bank << "." << gpio_number << "\tDONE" << endl;
        }
        else
        {
            f << "in";
            cout << "Setting direction IN "<< gpio_bank << "." << gpio_number << "\tDONE" << endl;
        }
    }
    else
    {
        cerr << "ERROR: Cannot open the direction file (B"<< gpio_bank << " P" << gpio_number << ")" << endl;
        return 1;
    }
    f.close();

    return 0;
}

int gpio_c::set_direction(t_dir direction)
{
    return set_direction(gpio_bank_priv,gpio_number_priv,direction);
}

int gpio_c::set_value(int gpio_bank, int gpio_number,int value)
{
    fstream f;
    
    const int abs_number =  gpio_bank * 32 + gpio_number;

    string path_direction;
    // PATH PARSING
    path_direction = GPIO_PATH "gpio";
    path_direction.append( get_gpionum(gpio_bank, gpio_number) );
    path_direction.append("/value");
    // END
    f.open(path_direction.c_str(),ios_base::out);

    if(f.is_open())
    {
            f << value;
            cout << "Setting value "<< value << "to GPIO " << gpio_bank << "." << gpio_number << "(" << abs_number  << ") DONE" << endl;
            }
    else
    {
        cerr << "ERROR: Cannot open the value file (B"<< gpio_bank << " P" << gpio_number << ")" << endl;
        return 1;
    }
    f.close();

    return 0;
}

int gpio_c::set_value(int gpio_bank, int value)
{
    fstream f;

    string path_direction;
    // PATH PARSING
    path_direction = GPIO_PATH "gpio";
    // END
    for(int i=0;i<31;i++)
    {
        path_direction.append( get_gpionum(gpio_bank, i) );
        path_direction.append("/value");
        f.open(path_direction.c_str(),ios_base::out);

        if(f.is_open())
        {
                f << value;
                cout << "Setting value "<< value << "to GPIO " << gpio_bank << "." << i+1 << " DONE" << endl;
                }
        else
        {
            cerr << "ERROR: Cannot open the value file (B"<< gpio_bank << ")" << endl;
            return 1;
        }
        f.close();
    }
    return 0;
}

int gpio_c::set_value(int value)
{
    return set_value(gpio_bank_priv,gpio_number_priv,value);
}

void gpio_c::test(const int iterations)
{
    cout << "Testing B" << gpio_bank_priv << " P" << gpio_number_priv << endl;
    for(int i=0;i<iterations;i++)
        {
            set_value(1);
            usleep(1000000);
            set_value(0);
            usleep(1000000);    
        }
    
}

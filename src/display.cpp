#include "display.h"

Display::Display(gpio_c* EPin, gpio_c* RSPin, gpio_c* DataBus[])
{
	DispEPin = EPin;
	DispRSPin = RSPin;

    for(int i=0;i<4;i++)
        DispDataBus[i]=DataBus[i];
}

void Display::ArrayWrite(gpio_c* InputArray[], const uint8_t value)
{
    for(int i=0;i<4;i++)
        if(value & (1 << i) )
            InputArray[i]->set_value(1);
        else
            InputArray[i]->set_value(0);
    
}

void Display::Clock()
{
    DispEPin->set_value(1);
    usleep(2000);
    DispEPin->set_value(0);
    usleep(1000);
}

void Display::Init()
{
    // set 4b mode
    ArrayWrite(DispDataBus, 0b0010); Clock();

    // st 4b operation 2 line display font 5x8
    ArrayWrite(DispDataBus,0b0010); Clock();
    ArrayWrite(DispDataBus,0b1000); Clock();

    // Turn on display and cursor
    ArrayWrite(DispDataBus,0b0000); Clock();
    ArrayWrite(DispDataBus, 0b1110); Clock();

    // Set increment address and shift cursor right
    ArrayWrite(DispDataBus, 0b0000); Clock();
    ArrayWrite(DispDataBus,0b0110); Clock();

    // Set cursor to 0 and clear
    ArrayWrite(DispDataBus,0b0000); Clock();
    ArrayWrite(DispDataBus, 0b0001); Clock(); 
    
}

void Display::WriteChar(const char Character)
{
    // set RS to "1"
    DispRSPin->set_value(1);

    // breaks character to nibbles
    char temp = (Character >> 4);

    ArrayWrite(DispDataBus, temp); Clock();
    temp = (Character & 0x0f);
    ArrayWrite(DispDataBus, temp); Clock();

    // set RS back to "0"
    DispRSPin->set_value(0);    
}

void Display::WriteInst(const uint8_t Inst)
{
    char temp = (Inst >> 4);

    ArrayWrite(DispDataBus, temp); Clock();
    temp = (Inst & 0x0f);
    ArrayWrite(DispDataBus, temp); Clock();

}

void Display::Write(const char * InputString, const uint8_t address)
{
    // go to address
    WriteInst((128 | address));

    // write whole string by char
     while(*InputString != '\0')
    {
        WriteChar(*InputString);
        InputString++;
    }

}
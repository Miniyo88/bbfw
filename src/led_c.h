#ifndef LED_C_H
#define LED_C_H

#define LED0		"/sys/class/leds/beaglebone:green:usr0/"
#define LED1		"/sys/class/leds/beaglebone:green:usr1/"
#define LED2		"/sys/class/leds/beaglebone:green:usr2/"
#define LED3		"/sys/class/leds/beaglebone:green:usr3/"

enum t_trigger { NONE,NAND,MMC0,MMC1,TIMER,ONESHOT,HEARTBEAT,BACKLIGHT,GPIO,CPU,DEFAULTON,TRANSIENT};

#include <iostream>
#include <fstream>

using namespace std;

class led_c
{
    fstream f;
    string path;				// path to /sys/class files

public:
        led_c(string arg_path);
        void set(string arg_path);			// changes the led file path
        int turn_on();						// turns it on
        int turn_off();						// turns it off
        string trigger_get();				// gets its triggers as string
        int trigger_set(string trigger);	// sets its trigger by string
        int trigger_set(t_trigger trigger);	// sets its trigger by string


};
#endif

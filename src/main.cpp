#include "main.h"
#include "led_c.h"
#include "Gpio.h"
#include "functions.h"
#include "gui.h"
#include "display.h"

#include <stdint.h>

using namespace std;

int main () 
{
    
    print_header("BBB Framework");


    print_header("Creating and assigning array of pointers to leds...");

    // creates array of pointers to user leds
    led_c* userLeds[4];
    userLeds[0] = new led_c(string(LED0));
    userLeds[1] = new led_c(string(LED1));
    userLeds[2] = new led_c(string(LED2));
    userLeds[3] = new led_c(string(LED3));

    print_header("Creating and assigning array of pointers to relays...");
    // array of pointers to relay
    // 

    Gpio* relays[4];
    // this code creates new member of class Gpio
    // assigns the gpio bank and pin number and direction
    relays[0] = new Gpio(3,3,DIR_OUT);    
    relays[1] = new Gpio(3,4,DIR_OUT);
    relays[2] = new Gpio(3,5,DIR_OUT);
    relays[3] = new Gpio(3,6,DIR_OUT);


    print_header("Creating and assigning display status pins...");
    // display driver pins
    Gpio* DispE = new Gpio(0,30,DIR_OUT);
    Gpio* DispRS = new Gpio(0,31,DIR_OUT);
   
    print_header("Creating and assigning display data pins...");
    // display data pins
    Gpio* dispD[4];
    dispD[0] = new Gpio(1,16,DIR_OUT);
    dispD[1] = new Gpio(1,28,DIR_OUT);
    dispD[2] = new Gpio(1,18,DIR_OUT);
    dispD[3] = new Gpio(1,19,DIR_OUT);

    print_header("Creating display object...");
    Display* display = new Display(DispE, DispRS, dispD);

    print_header("Initializing display...");
    display->Init();

    print_header("Writing text to display...");
    display->Write("BBB Framework",0x00);
    display->Write("(c)2014 L. Janik",0x28);

    print_header("Setting trigger \"NONE\" to LEDs...");
    // preset LEDs
    for(int i=0;i<4;i++)
    {
        userLeds[i]->trigger_set("none");
    }

    // deletes all created objects

        for(int i=0;i<4;i++)
        {
            delete(userLeds[i]);
            delete(relays[i]);
            delete(dispD[i]);
        }


    return 0;
}

#ifndef GUI_H
#define GUI_H

#include <iostream>

using namespace std;

void print_header(string text);
void print_text(string text);

int menu_main();
int menu_leds();
int menu_gpio();

#endif // GUI_H

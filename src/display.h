#ifndef DISPLAY_H
#define DISPLAY_H

#include <iostream>
#include "gpio_c.h"
#include <stdint.h>
#include <unistd.h>

enum t_display_instruction {CLEAR, RETURN};

using namespace std;

class Display
{	
private:
	// gpio_c* DispRWPin;
	gpio_c* DispEPin;
	gpio_c* DispRSPin;
	gpio_c* DispDataBus[4];

	void ArrayWrite(gpio_c* InputArray[], const uint8_t value);
	void Clock();
	void WriteInst(const uint8_t Inst);

public:
	Display(gpio_c* DispEPin, gpio_c* DispRSPin, gpio_c* DispDataBus[]);
	void Init();
	void WriteChar(const char Character);
	void Write(const char * InputString, const uint8_t address);
};


#endif
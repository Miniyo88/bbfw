#include <iostream>
#include "gui.h"
#include <unistd.h>

using namespace std;


void print_header(string text)
{
    cout <<" ";

    for(unsigned int i=0;i<text.length()+4;i++)
        cout << "-";

    cout << endl;
    cout << "|- " << text << " -|" << endl;
    cout << " ";

    for(unsigned int i=0;i<text.length()+4;i++)
        cout << "-";

    cout << endl;

}

void print_text(string text)
{
    cout << text << endl;
    usleep(2000);
}

int menu_main()
{
    int choice = 0;

    cout << "Choose action:" << endl;
    cout << "1. Work with LEDs" << endl;
    cout << "2. Work with GPIO" << endl;
    cout << endl;
    cout << "0. Exit "<< endl;
    cout << "Your choice: ";

    cin >> choice;

    return choice;
}

int menu_leds()
{
    int choice = 0;

    cout << "Choose LED:" << endl;
    cout << "1. User LED 1" << endl;
    cout << "2. User LED 2" << endl;
    cout << "3. User LED 3" << endl;
    cout << "4. User LED 4" << endl;
    cout << endl;
    cout << "0. Exit "<< endl;
    cout << "Your choice: ";

    cin >> choice;

    switch(choice)
    {
    case 1:
        cout << "vase volba: " << choice;
        menu_leds();
        break;
    case 2:
        cout << "vase volba: " << choice;
        menu_leds();
        break;
    case 3:
        cout << "vase volba: " << choice;
        menu_leds();
        break;

    case 4:
        cout << "vase volba: " << choice;
        menu_leds();
        break;
    default:
        break;
    }


    return choice;
}

int menu_gpio()
{
    int choice = 0;

    cout << "Choose GPIO:" << endl;

    cout << endl;
    cout << "0. Exit "<< endl;
    cout << "Your choice: ";

    cin >> choice;

    return choice;
}

#include "led_c.h"
#include "functions.h"
#include <string>
#include <cstring>


led_c::led_c(string arg_path)
{
    path = arg_path;
}

void led_c::set(string arg_path)
{
    path = arg_path;
}

string led_c::trigger_get()
{
    string read_buffer;
    string trigger_path = path;
    trigger_path.append("trigger");

#ifdef DEBUG
    cout << "Getting triggers..."<<endl;
    cout << "Opening "<<trigger_path<<"\t";
#endif

    f.open(trigger_path.c_str(), ios_base::in);

    if (f.is_open())
    {
        getline(f,read_buffer);

#ifdef DEBUG
        cout << "Available triggers: ";
        cout << read_buffer << endl;
#endif
        f.close();
        return read_buffer;
    }
    else
        error("cannot open file");
    return 0 ;
}

int led_c::trigger_set(string trigger)
{
    string trigger_path = path;
    trigger_path.append("trigger");


    cout << "Setting trigger \"" << trigger << "\" to LED " << path << endl;
    f.open(trigger_path.c_str(),ios_base::out);


    if (f.is_open())
    {
        cout <<" OK"<<endl;
        f<<trigger;
        f.close();

        return 0;
    }
    else
        cerr << "ERROR: setting LED trigger \"" << trigger << "\"" << endl;
    return 1;


}

int led_c::turn_on()
{
    string brightness_path = path;
    brightness_path.append("brightness");
#ifdef DEBUG
    cout << "Turning on LED " << path << "\t";
#endif
    f.open(brightness_path.c_str(),ios_base::out);


    if (f.is_open())
    {
        f<<1;
        f.close();

        return 0;
    }
    else
        error("Error while setting LED ON");
    return 1;

}

int led_c::turn_off()
{
    string brightness_path = path;
    brightness_path.append("brightness");
#ifdef DEBUG
    cout << "Turning off LED " << path << "\t";
#endif
    f.open(brightness_path.c_str(),ios_base::out);


    if (f.is_open())
    {
        f<<0;
        f.close();

        return 0;
    }
    else
        error("Error while setting LED OFF");
    return 1;

}

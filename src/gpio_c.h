#ifndef GPIO_C_H
#define GPIO_C_H

#include <iostream>
#include <fstream>

#define GPIO_PATH "/sys/class/gpio/"

enum Direction { DIR_OUT, DIR_IN };

using namespace std;

class Gpio
{
private:
    string path;
    int gpio_bank_priv;
    int gpio_number_priv;
    string get_gpionum(int bank,int number);   //<! calculates the gpio number - bank number begins at 1, gpio pin in bank begins at 1

public:
    Gpio(int gpioBank, int gpioNumber);          // constructor
    Gpio(int gpioBank, int gpioNumber, t_dir direction);          // constructor with setting direction
    ~Gpio();

    void set(int gpioBank,int gpioNumber);

    int exportb(int gpioBank, int gpioNumber);     // exports the gpio described by bank number and pin number
    int exportb(int gpioBank);                      // exports whole gpio bank
    int exportb();                                   // exports actual gpio

    int unexport(int gpioBank, int gpioNumber);  // -||-
    int unexport(int gpioBank);                     // unexports whole gpio bank
    int unexport();                                  //

    int setDirection(int gpioBank, int gpioNumber, Direction direction);  // sets gpio direction identified by bank number and pin number
    int setDirection(t_dir direction);                                 // sets gpio direction on actual gpio

    int setValue(int gpioBank, int gpioNumber,int value);    // sets value on specified gpio
    int setValue(int gpioBank, int value);                    // sets value on whole bank
    int setValue(int value);                                   // sets value on specified gpio

    void test(const int iterations);

};

#endif // GPIO_C_H
